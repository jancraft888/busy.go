package main

import (
  "flag"
  "log"
  "net/http"
  "crypto/sha256"
  "fmt"
  "os"
  "io"
  "bufio"
  "strings"
  "time"
  "sync"
  "strconv"
)

func Contains[T comparable](s []T, e T) bool {
  for _, v := range s {
    if v == e {
      return true
    }
  }
  return false
}

var addr = flag.String("addr", "0.0.0.0:13921", "address to listen at")
var rawValidOrigins = flag.String("origin", "http://localhost,https://localhost", "allowed origin(s)")
var validOrigins []string
var rootPath = flag.String("root", "/", "the root path to listen to")
var usersPath = flag.String("users", "users.conf", "the users file")
var retransmitTime = flag.Duration("retransmit", 5 * time.Second, "consumable retrasmit delay")
var pubUserHash = make(map[string]string)
var subUserHash = make(map[string]string)
var conUserHash = make(map[string]string)
var nc = NewNotificationCenter()
var consumableMap = make(map[int]*Event)
var consumableMapLock = sync.RWMutex{}
var NextEventID = 1

func Hash(str string) string {
  return fmt.Sprintf("%x", sha256.Sum256([]byte(str)))
}

func AddDefaultHeaders(w http.ResponseWriter, r *http.Request) {
  w.Header().Add("Content-Type", "text/plain; charset=utf-8")
  w.Header().Add("Cache-Control", "no-store")
  w.Header().Add("Server", "busygo")

  origin := r.URL.Scheme + "://" + r.URL.Host
  if Contains(validOrigins, origin) {
    w.Header().Add("Access-Control-Allow-Origin", origin)
  } else {
    w.Header().Add("Access-Control-Allow-Origin", validOrigins[0])
  }
  w.Header().Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
  w.Header().Add("Access-Control-Allow-Headers", "Content-Type, Authorization")
  w.Header().Add("Access-Control-Allow-Credentials", "true")
}

func HandlePrefilight(w http.ResponseWriter, r *http.Request) bool {
  if r.Method != http.MethodOptions { return false }

  AddDefaultHeaders(w, r)
  w.Header().Add("Vary", "Origin")
  w.Header().Add("Vary", "Access-Control-Request-Method")
  w.Header().Add("Vary", "Access-Control-Request-Headers")

  w.WriteHeader(204)

  return true
}

func ReadUsersFile() error {
  file, err := os.Open(*usersPath)
  if err != nil {
    return err
  }

  scanner := bufio.NewScanner(file)
  scanner.Split(bufio.ScanLines)

  for scanner.Scan() {
    line := scanner.Text()
    parts := strings.Split(line, ":")
    if strings.Contains(parts[0], "pub") {
      pubUserHash[parts[1]] = parts[2]
    }
    if strings.Contains(parts[0], "sub") {
      subUserHash[parts[1]] = parts[2]
    }
    if strings.Contains(parts[0], "con") {
      conUserHash[parts[1]] = parts[2]
    }
  }

  file.Close()
  return nil
}

func GetBodyString(r *http.Request) (string, error) {
  defer r.Body.Close()
  bodyBytes, err := io.ReadAll(r.Body)
  if err != nil {
    log.Fatal(err)
    return "", err
  }
  return string(bodyBytes), nil
}

func PublishMessage(event string, msg string) *Event {
  id := NextEventID
  NextEventID += 1
  ev := &Event{ Event: event, Data: msg, Id: id }
  nc.Notify(*ev)
  return ev
}

func Subscribe(w http.ResponseWriter, r *http.Request) {
  if HandlePrefilight(w, r) { return }
  AddDefaultHeaders(w, r)

  user, passwd, ok := r.BasicAuth()
  if !ok {
    w.WriteHeader(401)
    return
  }
  strtohash := "busygo:" + user + ":" + passwd
  hashed := Hash(strtohash)
  if tHash, ok := subUserHash[user]; !ok || tHash != hashed {
    w.WriteHeader(401)
    return
  }

  HandleSSE(nc, w, r)
}

func Publish(w http.ResponseWriter, r *http.Request) {
  if HandlePrefilight(w, r) { return }
  AddDefaultHeaders(w, r)

  user, passwd, ok := r.BasicAuth()
  if !ok {
    w.WriteHeader(401)
    return
  }
  strtohash := "busygo:" + user + ":" + passwd
  hashed := Hash(strtohash)
  if tHash, ok := pubUserHash[user]; !ok || tHash != hashed {
    w.WriteHeader(401)
    return
  }

  s, err := GetBodyString(r)
  if err != nil {
    w.WriteHeader(500)
    return
  }
  ev := PublishMessage(r.URL.Query().Get("event"), s)
  if r.URL.Query().Get("consumable") == "true" {
    consumableMapLock.Lock()
    consumableMap[ev.Id] = ev
    consumableMapLock.Unlock()
  }
  w.WriteHeader(200)
  fmt.Fprintf(w, "event: %s\nid: %d\ndata: %s\n\n", ev.Event, ev.Id, ev.Data)
}

func Consume(w http.ResponseWriter, r *http.Request) {
  if HandlePrefilight(w, r) { return }
  AddDefaultHeaders(w, r)

  user, passwd, ok := r.BasicAuth()
  if !ok {
    w.WriteHeader(401)
    return
  }
  strtohash := "busygo:" + user + ":" + passwd
  hashed := Hash(strtohash)
  if tHash, ok := conUserHash[user]; !ok || tHash != hashed {
    w.WriteHeader(401)
    return
  }

  evId := r.URL.Query().Get("id")
  evIdInt, err := strconv.Atoi(evId)
  if err != nil {
    w.WriteHeader(400)
    return
  }
  consumableMapLock.RLock()
  ev, ok := consumableMap[evIdInt]
  if !ok {
    w.WriteHeader(404)
    return
  }
  consumableMapLock.RUnlock()

  consumableMapLock.Lock()
  delete(consumableMap, evIdInt)
  consumableMapLock.Unlock()
  w.WriteHeader(200)
  fmt.Fprintf(w, "event: %s\nid: %d\ndata: %s\n\n", ev.Event, ev.Id, ev.Data)
}

func Consumables(w http.ResponseWriter, r *http.Request) {
  if HandlePrefilight(w, r) { return }
  AddDefaultHeaders(w, r)

  user, passwd, ok := r.BasicAuth()
  if !ok {
    w.WriteHeader(401)
    return
  }
  strtohash := "busygo:" + user + ":" + passwd
  hashed := Hash(strtohash)
  if tHash, ok := conUserHash[user]; !ok || tHash != hashed {
    w.WriteHeader(401)
    return
  }

  w.WriteHeader(200)
  consumableMapLock.RLock()
  for _, ev := range consumableMap {
    fmt.Fprintf(w, "event: %s\nid: %d\ndata: %s\n\n", ev.Event, ev.Id, ev.Data)
  }
  consumableMapLock.RUnlock()
}

func Retransmitter() {
  for {
    consumableMapLock.RLock()
    for _, ev := range consumableMap {
      nc.Notify(*ev)
    }
    consumableMapLock.RUnlock()
    time.Sleep(*retransmitTime)
  }
}

func main() {
  flag.Parse()
  log.SetFlags(0)

  if err := ReadUsersFile(); err != nil {
    panic(err)
  }

  validOrigins = strings.Split(*rawValidOrigins, ",")

  go Retransmitter()

  http.HandleFunc(*rootPath + "subscribe", Subscribe)
  http.HandleFunc(*rootPath + "publish", Publish)
  http.HandleFunc(*rootPath + "consume", Consume)
  http.HandleFunc(*rootPath + "consumables", Consumables)
  log.Printf("Listening on %s...\n", *addr)
  log.Fatal(http.ListenAndServe(*addr, nil))
}
