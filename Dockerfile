# syntax=docker/dockerfile:1
FROM golang:alpine
#FROM arm64v8/golang:alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./

RUN go mod download && go mod verify

COPY *.go ./

RUN go build -o /busy-go

CMD [ "/busy-go" ]
