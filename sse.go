package main

// Adapted from https://gist.github.com/rikonor/e53a33c27ed64861c91a095a59f0aa44

import (
  "fmt"
  "net/http"
  "sync"
  "strings"
  "time"
)

type UnsubscribeFunc func() error

type Event struct {
  Event string
  Id int
  Data string
}

type Subscriber interface {
  Subscribe(c chan Event) (UnsubscribeFunc, error)
}

func HandleSSE(s Subscriber, w http.ResponseWriter, r *http.Request) {
  // Subscribe
  c := make(chan Event)
  unsubscribeFn, err := s.Subscribe(c)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  body, err := GetBodyString(r)
  if err != nil {
    http.Error(w, err.Error(), http.StatusBadRequest)
    return
  }
  validEvents := strings.Split(body, "\n")

  // Signal SSE Support
  w.Header().Set("Content-Type", "text/event-stream")
  w.Header().Set("Cache-Control", "no-cache")
  w.Header().Set("Connection", "keep-alive")
  w.WriteHeader(200)
  w.(http.Flusher).Flush()

  Looping:
  for {
    select {
    case <-r.Context().Done():
      if err := unsubscribeFn(); err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
      }
      break Looping

    default:
      e := <-c
      if e.Event == "$$__heartbeat__$$" {
        fmt.Fprintf(w, ": keep-alive\n\n")
        w.(http.Flusher).Flush()
      }
      if Contains(validEvents, e.Event) {
        fmt.Fprintf(w, "event: %s\nid: %d\ndata: %s\n\n", e.Event, e.Id, e.Data)
        w.(http.Flusher).Flush()
      }
    }
  }
}

type Notifier interface {
  Notify(e Event) error
}

type NotificationCenter struct {
  subscribers   map[chan Event]struct{}
  subscribersMu *sync.Mutex
}

func NewNotificationCenter() *NotificationCenter {
  nc := &NotificationCenter{
    subscribers:   map[chan Event]struct{}{},
    subscribersMu: &sync.Mutex{},
  }

  go func() {
    for {
      nc.KeepAlive()
      time.Sleep(10 * time.Second)
    }
  }()

  return nc
}

func (nc *NotificationCenter) Subscribe(c chan Event) (UnsubscribeFunc, error) {
  nc.subscribersMu.Lock()
  nc.subscribers[c] = struct{}{}
  nc.subscribersMu.Unlock()

  unsubscribeFn := func() error {
    nc.subscribersMu.Lock()
    delete(nc.subscribers, c)
    nc.subscribersMu.Unlock()

    return nil
  }

  return unsubscribeFn, nil
}

func (nc *NotificationCenter) Notify(e Event) error {
  nc.subscribersMu.Lock()
  defer nc.subscribersMu.Unlock()

  for c := range nc.subscribers {
    select {
    case c <- e:
    default:
    }
  }

  return nil
}
func (nc *NotificationCenter) KeepAlive() error {
  nc.subscribersMu.Lock()
  defer nc.subscribersMu.Unlock()

  e := Event{ Event: "$$__heartbeat__$$" }

  for c := range nc.subscribers {
    select {
    case c <- e:
    default:
    }
  }

  return nil
}
