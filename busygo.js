export class PostingEventSource {
  constructor(url, options) {
    this.url = url
    this.headers = options.headers ?? {}
    this.payload = options.payload ?? ''
    this.type = options.type ?? 'application/json'
    this.listeners = {}
    this.xhr = null
    this.readyState = EventSource.CONNECTING
    this.progress = 0
    this.chunk = ''
  }

  addEventListener(type, listener) {
    if (!(type in this.listeners)) this.listeners[type] = []
    if (this.listeners[type].indexOf(listener) == -1) this.listeners[type].push(listener)
  }

  removeEventListener(type, listener) {
    if (!(type in this.listeners)) return
    this.listeners[type] = this.listeners.filter(v => v != listener)
  }

  dispatchEvent(e) {
    if (!e) return true
    e.source = this
    if (e.type in this.listeners) {
      return this.listeners[e.type].every(f => {
        f(e)
        return !e.defaultPrevented
      })
    }
    return true
  }

  _dispatchReadyState(s) {
    let event = new CustomEvent('readystatechange')
    event.readyState = s
    this.readyState = s
    this.dispatchEvent(event)
  }

  close() {
    if (this.readyState == EventSource.CLOSED) return

    this.xhr.abort()
    this.xhr = null
    this._dispatchReadyState(EventSource.CLOSED)
  }

  _parseChunk(c) {
    if (!c || c.length == 0) return null

    const e = { data: '', event: 'message', id: -1 }
    c.split(/\n|\r\n|\r/).forEach(l => {
      l = l.trim()
      const i = l.indexOf(':')
      if (i <= 0) return

      let f = l.substring(0, i)
      if (!(f in e)) return

      let v = l.substring(i + 1).trim()
      if (f == 'data')
        e[f] += v
      if (f == 'id')
        e[f] = parseInt(v)
      else e[f] = v
    })
    const event = new CustomEvent(e.event, { detail: e })
    event.data = e.data
    return event
  }

  _streamProgress(ev) {
    if (!this.xhr) return

    if (this.xhr.status != 200) return
    if (this.readyState == this.CONNECTING) {
      this.dispatchEvent(new CustomEvent('open'))
      this._dispatchReadyState(EventSource.OPEN)
    }

    const d = this.xhr.responseText.substring(this.progress)
    this.progress += d.length
    d.split(/(\r\n|\r|\n){2}/g).forEach(p => {
      if (p.trim().length == 0) {
        this.dispatchEvent(this._parseChunk(this.chunk.trim()))
        this.chunk = ''
      } else
        this.chunk += p
    })
  }

  stream() {
    this.xhr = new XMLHttpRequest()
    this.xhr.addEventListener('progress', this._streamProgress.bind(this))
    this.xhr.addEventListener('load', ev => {
      this._streamProgress(ev)
      this.dispatchEvent(this._parseChunk(this.chunk))
      this.chunk = ''
    })

    this.xhr.addEventListener('readystatechange', ev => {
      if (!this.xhr) return
      if (this.xhr.readyState === XMLHttpRequest.DONE) this._dispatchReadyState(EventSource.CLOSED)
    })
    this.xhr.open('POST', this.url)
    for (const header in this.headers)
      this.xhr.setRequestHeader(header, this.headers[header])
    this.xhr.send(this.payload)
  }
}


export class BusyGoBus {
  constructor(url, auth) {
    this.url = url
    this.pev = null
    this.handlers = {}
    this.subqueue = []
    this._auth = "Basic " + btoa(auth)
    this.close_h = console.info
  }

  on(event, handler) {
    this.handlers[event] = handler
    if (this.pev) this.pev.addEventListener(event, this._handle.bind(this))
  }

  close() {
    this.pev?.close()
  }

  onClose(handler) {
    this.close_h = handler
  }

  _handle(ev) {
    if (ev.type in this.handlers)
      this.handlers[ev.type](ev.detail)
  }

  subscribeTo(events=[]) {
    this.pev = new PostingEventSource(`${this.url}/subscribe`, { type: 'text/plain', headers: { Authorization: this._auth }, payload: events.join('\n') })
    this.pev.addEventListener('readystatechange', (ev) => {
      if (this.pev.readyState == EventSource.CLOSED) this.close_h(ev)
    })
    for (const ev in this.handlers) {
      this.pev.addEventListener(ev, this._handle.bind(this))
    }
    this.pev.stream()
  }

  consume(id) {
    return fetch(`${this.url}/consume?id=${id}`, {
      headers: {
        Authorization: this._auth
      }
    })
  }

  async consumables() {
    const f = await fetch(`${this.url}/consumables`, {
      method: 'GET',
      headers: {
        Authorization: this._auth
      }
    })
    const d = await f.text()

    return d.split(/(\r\n|\r|\n){2}/g).map(this.pev._parseChunk).filter(o => o && o.data != '').map(x => x.detail)
  }

  publish(event, payload, consumable=false) {
    return fetch(`${this.url}/publish?${consumable ? 'consumable=true&' : ''}event=${event}`, {
      method: 'POST',
      headers: {
        Authorization: this._auth
      },
      body: JSON.stringify(payload)
    })
  }
}
