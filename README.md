![busy.go banner](/assets/busygo_banner.png)

# busy.go
Realtime message bus written in Go.

> **⚠️ Warning!** Version 2 uses a different networking solution, please refer to the `old-v1` branch if needed.

## Building
Just build it like any Go app, you may also install it to your GOPATH.

```shell
go build
go install
```

## Usage
```shell
busy-go -h
```

| Flag       | Default                             | Info                        |
|------------|-------------------------------------|-----------------------------|
| help       |                                     | Shows help message          |
| addr       | 0.0.0.0:13921                       | Address to listen at        |
| origin     | http://localhost, https://localhost | Allowed origin(s)           |
| root       | /                                   | The root path to listen to  |
| users      | users.conf                          | The users file              |
| retransmit | 5s                                  | Consumable retransmit delay |

## Security

> **⚠️ Warning!** You should *always* put **busy.go** behind a reverse proxy and ensure appropiate rate limiting is applied.
> Lack of rate limiting can cause spamming, DoS-ing and brute-forcing passwords.

Authorization is done through a `Authorization: Basic` header. The users file defines users, their permissions and their password.
Permissions can be `pub`, `sub`, `con` or any combination of the three. The format is as follows:

```
pubsubcon:root:5cdf54d4e16d10c2298d1052923b149a50cb6187b2eeda772b854d5ca5869811
```

The hash can be obtained by running the following command (where `user` is the username and `password` is the password):
```sh
echo -n busygo:user:password | openssl sha256
```

> **⚠️ Warning!** Never use weak passwords. Or, even worse, default passwords.

## JavaScript
There's a JS library included which allows you to subscribe easily from the frontend.

```js
// 1st argument is base URL
// 2nd argument is Authorization
const bus = new BusyGoBus("http://localhost:13921", "user:test")

// register event handler for message
bus.on("chat-message", (event) => {
  console.log(event)
})

// must be an array of message types
bus.subscribeTo([ "chat-message" ])

// consume the event with ID 21
bus.consume(21)

// get a list of consumables
console.log(await bus.consumables())

// this line can be used in a Node/Deno/Bun server environment
// NEVER allow clients to publish messages unless you want pure chaos
bus.publish("chat-message", { "foo": "bar" })
```

## Docker
If you're using the [Docker image](https://hub.docker.com/r/jancraft888/busy-go), you must change the command to include the required flags.

To use `v2` (current version) you have to use the `jancraft888/busy-go:v2` tag.
